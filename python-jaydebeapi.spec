%global desc    \
The JayDeBeApi module allows you to connect from Python code to databases using\
Java JDBC. It provides a Python DB-API v2.0 to that database.

Name:           python-jaydebeapi
Version:        1.2.3
Release:        1
Summary:        Bridge from JDBC database drivers to Python DB-API

License:        'LGPL-3.0-only', 'GPL-3.0-only'
URL:            https://github.com/baztian/jaydebeapi
Source0:        https://github.com/baztian/jaydebeapi/archive/v1.2.3/jaydebeapi-1.2.3.tar.gz

BuildArch:      noarch

BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-setuptools

%description
%{desc}

%package -n python%{python3_pkgversion}-jaydebeapi
Summary:        Bridge from JDBC database drivers to Python DB-API
%{?python_provide:%python_provide python%{python3_pkgversion}-jaydebeapi}

Requires:       python%{python3_pkgversion}-jpype

%description -n python%{python3_pkgversion}-jaydebeapi
%{desc}

%prep
%autosetup -n jaydebeapi-1.2.3

%build
%py3_build

%install
%py3_install

%files -n python%{python3_pkgversion}-jaydebeapi
%license COPYING COPYING.LESSER
%doc README.rst README_development.rst
%{python3_sitelib}/jaydebeapi/
%{python3_sitelib}/JayDeBeApi*

%changelog
* Fri Jul 09 2021 xuyonghui <xuyonghui@kylinos.cn> - 1.2.3-1
- Package init
